import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    var a = value.split('||');
    if(a.length > 0){
      return a[1]
    } else{
      return value;
    }
    
  }

}
