import { Component, OnInit } from '@angular/core';
import { UserDetails } from '../user-details';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
user = new UserDetails();

  constructor() {
    this.user = JSON.parse(localStorage.getItem("key"));
   }

  ngOnInit() {
  }

}
