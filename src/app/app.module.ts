import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { SigninComponent } from './signin/signin.component';
import { AuthUserService } from './auth-user.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { FormsModule } from '@angular/forms';
import { UserDetails} from './user-details';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular-6-social-login";
import { AuthenticationService } from './authentication.service';
import { MyPipePipe } from './my-pipe.pipe';
import { HeaderComponent } from './header/header.component';
import { MyDirectiveDirective } from './my-directive.directive';


export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("224889169940-kltbp3595h4sf82oj92h69h11gkqle5v.apps.googleusercontent.com")
        },
      ]);
    return config;
  }

const route:Routes=[

  {
    path:   '',
    component:SigninComponent
  },
{
  path:   'signin',
  component:SigninComponent
},
{
  path:   'main',
  component:MainComponent,
  canActivate: [AuthenticationService]
}

];
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SigninComponent,
    MyPipePipe,
    HeaderComponent,
    MyDirectiveDirective
  ],
  imports: [
    BrowserModule, 
    HttpClientModule,
    FormsModule,
    SocialLoginModule,
    RouterModule.forRoot(route)
  ],
  providers: [AuthUserService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
