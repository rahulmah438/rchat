import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from '../../node_modules/rxjs';
import { Data } from './data';
import { pipe } from '../../node_modules/@angular/core/src/render3/pipe';
import { map } from 'rxjs/operators';
import { UserDetails } from './user-details'


@Injectable({
  providedIn: 'root'
})

export class AuthUserService implements OnInit {
  ngOnInit() {
    
  }
  user = new UserDetails();

  httpOptions = {
    headers : new HttpHeaders ({
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization' : 'Basic QUM1ZjExM2U5MDMyYTk1NDhkYWZiOTMwOWVjMWU5NDZkNzphNWNjNTVkZDlmOWI0ZTQ1ZDdkNThhN2ZjZDEyMDAyYg=='
    })
  };
  
// UserName : string = 'AC5f113e9032a9548dafb9309ec1e946d7';
// Password : string = 'a5cc55dd9f9b4e45d7d58a7fcd12002b';
// ServiceId : string =  'IS612939f0af5d47e9a5867679dfa372f3';
url = 'https://chat.twilio.com/v2/Services'
  constructor(private http : HttpClient) { 
    this.user = JSON.parse(localStorage.getItem("key"));
    console.log(this.user.email);
  }
  SetData():Observable<any> {
    return this.http.post(this.url,'FriendlyName=Rahul', this.httpOptions);
  }
  CreateChannel():Observable<any> {
    return this.http.post('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels', 'UniqueName=General', this.httpOptions);
  }
  DisplayAllChannel():Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels', this.httpOptions);
  }
  AddChannel(str):Observable<any> {
    return this.http.post('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels', 'UniqueName='+str, this.httpOptions);
  }
  RetrieveChannelId(str):Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels/'+str, this.httpOptions);
  }
  RetrieveChannelName(str):Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels/'+str, this.httpOptions);
  }
  ChannelAddMember(str):Observable<any> {
    return this.http.post('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels/'+str+'/Members', 'ChannelSid='+str+'&Identity='+this.user.email+'&ServiceSid=IS6fc5291ab2ed4d1c8327dff1f2c0408b', this.httpOptions);
  }
  AddRole():Observable<any> {
    return this.http.post('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Roles',  'FriendlyName=Rahul&Permission=createChannel&Type=deployment', this.httpOptions);
  }
  
  AddMember():Observable<any> {
    return this.http.post('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels/CH5daad0a180c347da8c05516400916fb7/Members', 'ChannelSid=CH5daad0a180c347da8c05516400916fb7&Identity=rahul.maheshwari@kelltontech.com&ServiceSid=IS6fc5291ab2ed4d1c8327dff1f2c0408b', this.httpOptions);
  }
  ListAllUsers():Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Users', this.httpOptions);
  }
  RetrieveUser():Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Users/'+this.user.email, this.httpOptions);
  }
  IsSubscribed(str) :Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Users/'+str+'/Channels', this.httpOptions);
  }
  SendMessage(message, channel):Observable<any> {
    return this.http.post('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels/'+channel+'/Messages',  'ChannelSid=CH5daad0a180c347da8c05516400916fb7&ServiceSid=IS6fc5291ab2ed4d1c8327dff1f2c0408b&From='+this.user.email+'&Body='+message, this.httpOptions);
  }
  ShowAllMessages(str):Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Channels/'+str+'/Messages', this.httpOptions);
  }
  DetailUser():Observable<any> {
    return this.http.get('https://chat.twilio.com/v2/Services/IS6fc5291ab2ed4d1c8327dff1f2c0408b/Users/'+this.user.email, this.httpOptions);
  }

  
  }
  
  

