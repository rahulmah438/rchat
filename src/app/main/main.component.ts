import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthUserService } from '../auth-user.service';
import { Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgModel } from '@angular/forms';
import { UserDetails} from '../user-details';
import { Alert } from '../../../node_modules/@types/selenium-webdriver';
import { Router } from '@angular/router';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
 ngOnInit(){
    this.AuthObject.RetrieveUser().subscribe(res => 
    { 
      console.log(res);
      this.AuthObject.IsSubscribed(res.sid).subscribe(res => 
        { 
          this.length = res.channels.length;
          for ( let i = 0; i < this.length; i++){ 
              
            this.ChannelId[i] = res.channels[i].channel_sid;
            console.log(this.ChannelId);
          }
          for ( let i = 0; i < this.length; i++){ 
              
            this.AuthObject.RetrieveChannelName(this.ChannelId[i]).subscribe(res => 
              { 
                // this.ChannelName=res;
                this.ChannelName[i]=res.unique_name;
                console.log(this.ChannelName[i]);
            }),
              err => {
                console.log(err);
           } }
        }),
        err => {
          console.log(err);
        }
    }),
              err => {
                console.log(err);
              }
             
        this.AuthObject.AddMember().subscribe(res => 
          { 
          console.log(res); 
          });
          err => {
          alert('Welcome Again',);
         }
        
    this.AuthObject.AddRole().subscribe(res => 
      { 
        console.log(res); 
      }),
      err => {
        console.log(err);
      }
}  
    //   this.AuthObject.SetData().subscribe(res => 
    //   { 
    //     console.log(res); 
    //   }),
    //   err => {
    //     console.log(err);
    //   }
     

    // ButtonClicked() {
    //   this.AuthObject.CreateChannel().subscribe(res => 
    //        { 
    //          console.log(res); 
    //        }),
    //        err => {
    //          console.log(err);
    //        }
    //      }  
  details = JSON.parse(localStorage.getItem("key"));
  length : number;
  msgArr : Array<{body: any, from : string, frombracket : string, time: string}> = [];
  ChannelArr = [];
  MyVar : string;
  Msg : string;
  ChannelId = [];
  ChannelName = [];
  channel : string;
  user = new UserDetails();
  choice : string;
  txt : string;

  constructor(private AuthObject : AuthUserService, private http : HttpClient, private route : Router) {
    this.channel = localStorage.getItem("ChannelName");
    this.user = JSON.parse(localStorage.getItem("key"));
    this.AuthObject.ShowAllMessages('CH5daad0a180c347da8c05516400916fb7').subscribe(res => 
      { 
        this.length = res.messages.length;
        console.log(res);
        for ( let i = 0; i < this.length; i++){ 
          
        this.msgArr.push({
          body : res.messages[i].body,
          frombracket : '(' + res.messages[i].from + ')',
          from : res.messages[i].from,
          time : res.messages[i].date_created
        }) 
      
      }
      console.log(this.msgArr)
      }),
      err => {
        console.log(err);
      }
      
}  
  
  
  // UserAdd(str){
  //   this.AuthObject.AddUser(str).subscribe(res => 
  //     { 
  //       console.log(res); 
  //     }),
  //     err => {
  //       console.log(err);
  //     }
  // }
  // For searching a particular channel
  AddChannelIconClicked() {
    this.txt = prompt("Please enter channel name", "");
    if (this.txt == null || this.txt == "") {
      alert("you did not enter any channel name");
      return;
      }
      else {
      this.choice = this.txt;
      this.AuthObject.AddChannel(this.choice).subscribe(res => 
              { 
                alert("Channel successfully created");
                this.AuthObject.DisplayAllChannel().subscribe(res => 
                  { 
                    this.length = res.channels.length;
                    for ( let i = 0; i < this.length; i++){
                        this.ChannelArr[i] = res.channels[i].unique_name;
                    }
                      }),
                      err => {
                        console.log(err);
                      }
              }),
              err => {
                console.log(err);
              }
       } }
  
  SearchButtonClicked(str){
    this.AuthObject.DisplayAllChannel().subscribe(res => 
      { 
        this.length = res.channels.length;
        for ( let i = 0; i < this.length; i++){
          if (str === res.channels[i].unique_name ) {
            this.MyVar = res.channels[i].unique_name;
            break;
            }
          else {
            this.MyVar="Channel not found";
            console.log("Searched Group not found");
          }}
      }),
      err => {
        console.log(err);
      }
  }
 
 
  DisplayAllChannels(){
    this.AuthObject.DisplayAllChannel().subscribe(res => 
      { 
        console.log(res);
        this.length = res.channels.length;
        for ( let i = 0; i < this.length; i++){
            this.ChannelArr[i] = res.channels[i].unique_name;
        }
          }),
      err => {
        console.log(err);
      }
  }
 UserSid : string;
  DetailButtonClicked() {
    this.AuthObject.DetailUser().subscribe(res => 
     { 
     this.UserSid = res.sid;
     this.AuthObject.IsSubscribed(this.UserSid).subscribe(res =>
      {
      console.log(res);
       });
       err => {
         console.log(err);
       }
     });
     err => {
     console.log(err);
    }
   }
   
   AlertBoxUnread() {
     alert("You have no unread messages");
   }
   AlertBoxThread() {
    alert("You have no threads");
  }
    
   
    //      ButtonClicked() {
    //       this.AuthObject.DisplayAllChannel().subscribe(res => 
    //                 { 
    //                   this.length = res.channels.length;
    //                   for ( let i = 0; i < this.length; i++){ 
    //                   console.log(res.channels[i].unique_name);} 
    //                 }),
    //                 err => {
    //                   console.log(err);
    //                 }
    //          } 
          
    Send(str) {
          this.AuthObject.SendMessage(str, this.channel).subscribe(res => 
                    { 
                      this.Msg = '';
                      this.AuthObject.ShowAllMessages(this.channel).subscribe(res => 
                        { 
                          this.msgArr.length =0;
                          this.length = res.messages.length;
                          console.log(res);
                          for ( let i = 0; i < this.length; i++){ 
                            
                            this.msgArr.push({
                              body : res.messages[i].body,
                              frombracket : '(' + res.messages[i].from + ')',
                              from : res.messages[i].from,
                              time : res.messages[i].date_created
                          })
                         }}),
                        err => {
                          console.log(err);
                        }
                    }),
                    err => {
                      console.log(err);
                    }
             } 
             
    // History() {
    //       this.AuthObject.ShowAllMessages().subscribe(res => 
    //                 { 
    //                   this.length = res.messages.length;
    //                   console.log(res);
    //                   for ( let i = 0; i < this.length; i++){ 
                        
    //                   this.msgArr[i] = res.messages[i].body +'(' + res.messages[i].from + ')'; 
    //                   // this.msgArr.reverse;
    //               }
    //                 }),
    //                 err => {
    //                   console.log(err);
    //                 }
    //          }     
             AllChannel(str) {
              this.AuthObject.RetrieveChannelId(str).subscribe(res => 
               { 
                this.AuthObject.ChannelAddMember(res.sid).subscribe(res => 
                  { 
                        alert("You added to this channel successfully")
                  }),
                  err => {
                    console.log(err);
                  }
                 });
                 err => {
                   console.log(err);
                 }
               
             }
            

                      ViewChannelMessages(str) {
                        this.channel = str;
                        localStorage.setItem("ChannelName", this.channel)
                        this.AuthObject.RetrieveChannelId(str).subscribe(res => 
                         { 
                          this.AuthObject.ShowAllMessages(res.sid).subscribe(res => 
                            { 
                              this.msgArr.length =0;
                              this.length = res.messages.length;
                              console.log(res);
                              for ( let i = 0; i < this.length; i++){ 
                                
                                this.msgArr.push({
                                  body : res.messages[i].body,
                                  frombracket : '(' + res.messages[i].from + ')',
                                  from : res.messages[i].from,
                                  time : res.messages[i].date_created
                              })
                             }}),
                            err => {
                              console.log(err);
                            }
                           });
                           err => {
                             console.log(err);
                           }
                         
                       }
                      //  initChannelEvents() {
                      //    tc = {}
                      //   console.log(tc.currentChannel.friendlyName + ' ready.');
                      //   tc.currentChannel.on('messageAdded', tc.addMessageToList);
                      //   tc.currentChannel.on('typingStarted', showTypingStarted);
                      //   tc.currentChannel.on('typingEnded', hideTypingStarted);
                      //   tc.currentChannel.on('memberJoined', notifyMemberJoined);
                      //   tc.currentChannel.on('memberLeft', notifyMemberLeft);
                      //   $inputText.prop('disabled', false).focus();
                      // }
                      Logout(){
                        localStorage.clear();
                        this.route.navigate(['/Signin'])
                      }
    
            }






      
