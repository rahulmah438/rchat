import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appMyDirective]'
})
export class MyDirectiveDirective {

  constructor(private ref : ElementRef) {
    this.changeColor('#F5D0A9')
   }
   changeColor(color : any) {
     this.ref.nativeElement.style.color = color;
   }
}
